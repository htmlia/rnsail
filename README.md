# RNSail #

React Native Sail is starter for React Native project
This starter based on 
https://medium.com/@kevinle/comprehensive-routing-and-navigation-in-react-native-made-easy-6383e6cdc293

### What is this repository for? ###

* Starter for React Native App
* Version:0.1.0
* [Learn Markdown](https://bitbucket.org/sunaryohadi/rnsail/overview)

### How do I get set up? ###

* git pull
* cd to your dir
* yarn
* react-native run-ios [or] react-native run-android

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* www.htmlia.com